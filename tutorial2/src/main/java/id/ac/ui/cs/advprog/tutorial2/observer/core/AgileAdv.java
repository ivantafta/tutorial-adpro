package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class AgileAdv extends Adventurer {
    public AgileAdv(String name, Guild guild) {
        super(name, guild);
    }

    public void update() {
        if (this.guild.getQuestType().equalsIgnoreCase("D") || this.guild.getQuestType().equalsIgnoreCase("R")) {
            this.getQuests().add(this.guild.getQuest());
        }
    }
}