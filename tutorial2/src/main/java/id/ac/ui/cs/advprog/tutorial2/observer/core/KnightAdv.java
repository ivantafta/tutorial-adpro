package id.ac.ui.cs.advprog.tutorial2.observer.core;

public class KnightAdv extends Adventurer {
    public KnightAdv(String name, Guild guild) {
        super(name, guild);
    }

    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }
}
